# nobl9-action

This action is applying a Nobl9 configuration file (specified via the `SLOCTL_YML` input parameter) to a project using the  `sloctl` (https://docs.nobl9.com/sloctl-user-guide) tool.

# Requirements

- A valid Nobl9 account (see https://nobl9.com for more information)

# Usage

This action supports GitLab pipeline secrets in all the input parameters. More details: https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project.

Example:
```yaml
variables:
  CLIENT_ID: $NOBL9_CLIENT_ID
  CLIENT_SECRET: $NOBL9_CLIENT_SECRET
  ACCESS_TOKEN: $NOBL9_ACCESS_TOKEN
  PROJECT: $NOBL9_PROJECT
  SLOCTL_YML: $SLOCTL_YML

include:
  - project: 'nobl9/nobl9-ci-template'
    ref: main
    file: '/nobl9.gitlab-ci.yml'
```
